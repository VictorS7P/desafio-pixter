# Teste de desenvolvimento Front End

### Rodando o código

Basta clonar o repositório e rodar comandos:  
`npm i`  
`npm start`

Eu também fiz deploy no Heroku, basta só [clicar aqui](http://desafio-pixter.herokuapp.com/)