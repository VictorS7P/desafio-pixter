import React from 'react'
import { BrowserRouter as Router, Switch, Redirect, Route } from 'react-router-dom'

import Nav from 'components/Nav'
import Footer from 'components/Footer'
import DigitalBooks from 'screens/DigitalBooks'

const App = () => (
	<Router>
		<div className="bg-yellow">
			<div className="container">
				<header>
					<Nav />
				</header>
			</div>

			<Switch>
				<Route exact path="/" component={ DigitalBooks } />
				<Redirect to="/" />
			</Switch>

			<div className="bg-black">
				<div className="container">
					<Footer />
				</div>
			</div>
		</div>
	</Router>
)

export default App