import React from 'react'

import Carroussel from './Carroussel/'
import BooksArea from './BooksArea/'

export default () =>
<div>
    <Carroussel />
    <BooksArea />
</div>