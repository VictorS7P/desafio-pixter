import React from 'react'

export default ({ volumeInfo: { imageLinks: imgs }, onClick }) =>
<div className="text-center py-4">
    <img onClick={ onClick } src={ imgs && imgs.smallThumbnail } alt=""/>
</div>