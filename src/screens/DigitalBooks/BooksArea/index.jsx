import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Book from './Book'
import Modal from './Modal'

import { Creators as booksActions } from 'ducks/books'

const BooksArea = ({ books, loading, listRequest }) => {
    useEffect(() => {
        listRequest('HARRY%20POTTER')
    }, [ listRequest ])

    const [ showBook, toggleBook ] = useState(null)

    return (
        <>
            <div className="bg-white">
                <div className="container py-5">
                    <div className="row">
                        <div id="books" className="col-12 col-lg-10 mx-auto text-center">
                            <h3 className="mt-5 font-weight-bold f-xl"> Books </h3>

                            <p className="my-4 f-md text-dark-gray">
                                Minim aliqua sit dolore incididunt reprehenderit. Ullamco ex magna excepteur proident sint sunt et elit enim commodo aute consequat. In sunt nulla eiusmod magna tempor nulla sit dolor et voluptate.
                            </p>

                            <div className="row py-3">
                                { loading ? (
                                    <div className="col-12 text-center">
                                        <span> carregando... </span>
                                    </div>
                                ) : books.map((book, i) => (
                                    <div className="col-6 col-lg-3" key={`book-${ i }`}>
                                        <Book onClick={ () => toggleBook(book) } { ...book } />
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <Modal opened={ showBook !== null } close={ () => toggleBook(null) } book={ showBook } />
        </>
    )
}

const mapState = state => ({
    books: state.books.list,
    loading: state.books.listLoading
})

const mapDispatch = dispatch => bindActionCreators({
    ...booksActions
}, dispatch)

export default connect(mapState, mapDispatch)(BooksArea)