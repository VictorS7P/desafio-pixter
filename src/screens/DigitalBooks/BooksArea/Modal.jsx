import React from 'react'
import s from 'styled-components'
import { Transition } from 'react-transition-group'

import { setScroll } from 'commom/functions'
import Modal, { Close } from 'UI/Modal'

const Text = s.p`
    height: 250px;
`

export default ({ book, close, opened }) =>
<Transition in={ opened } timeout={ 300 } onEntered={ () => setScroll(false) } onExited={ () => setScroll(true) }>
    { state => (
        <Modal close={ close } state={ state }>
            <Close className="click f-lg" onClick={ close }> &times; </Close>
        
            { book && (
                <div className="px-4 pt-3">
                    <h4 className="font-weight-bold text-dark-gray">
                        { book.volumeInfo.title }
                    </h4>
        
                    <p className="f-sm d-inline-block py-1 px-3 bg-dark-gray text-white rounded">
                        { book.volumeInfo.publishedDate }
                    </p>
        
                    <Text className="mt-3 pb-5 pr-4 f-sm lead overflow-auto">
                        { book.volumeInfo.description }
                    </Text>
                </div>
            )}
        </Modal>
    )}
</Transition>