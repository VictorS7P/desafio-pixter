import React from 'react'

import ipad from 'assets/images/ipad.png'
import apple from 'assets/images/icons/apple.png'
import android from 'assets/images/icons/android.png'
import windows from 'assets/images/icons/windows.png'

const ICONS = [ apple, android, windows ]

export default () =>
<div className="row align-items-center">
    <div className="col-12 col-lg-7">
        <h2 className="font-weight-bold"> Pixter Digital Books </h2>

        <p className="f-md mt-3 text-dark-gray">
            Lorem ipsum dolor sit amet? <br />
            consecteur elit, volutpat.
        </p>

        <p className="f-sm text-light-gray-2">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br />
            Fusce in mi elit. Sed fermentum efficitur orci. <br />
            Duis tempor faucibus venenatis. Quisque nec efficitur metus, et iaculis velit. <br />
            Aliquam sed lacus ante. Donec vitae orci sem.
        </p>

        <div className="d-flex mt-4">
            { ICONS.map((el, i) => (
                <div key={`icon-${ i }`} className="mr-4">
                    <img src={ el } alt=""/>
                </div>
            ))}
        </div>
    </div>

    <div className="col-12 col-lg-5">
        <img src={ ipad } alt="" width="316" height="470" className="d-block mx-lg-auto mt-4 mt-lg-0" />
    </div>
</div>