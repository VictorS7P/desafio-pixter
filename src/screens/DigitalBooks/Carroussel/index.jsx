import "./Carroussel.scss"
import React, { useState, useEffect } from 'react'
import { Transition } from 'react-transition-group'

import Item1 from './Item1'
const ITEMS = [ Item1, Item1, Item1 ]

const itemStyle = {
    entering: { display: 'block', opacity: '0' },
    entered:  { display: 'block', opacity: '1' },
    exiting:  { display: 'block', opacity: '0' },
    exited:   { display: 'none',  opacity: '0' }
}

export default () => {
    const [ activeItem, toggleItem ] = useState(0)
    
    useEffect(() => {
        const interval = setInterval(() => {
            toggleItem(active => active === (ITEMS.length - 1) ? 0 : active + 1);
        }, 5000)

        return () => clearInterval(interval)
    }, [])

    return (
        <div className="container py-4">
            <div className="item-wrapper">
                { ITEMS.map((El, i) => (
                    <Transition key={`carroussel-${ i }`} in={ activeItem === i } timeout={ 300 }>
                        { state => (
                            <div style={ itemStyle[state] } className="transition w-100 position-absolute">
                                <El />
                            </div>
                        )}
                    </Transition>
                ))}
            </div>

            <div className="dots">
                { ITEMS.map((_, i) => (
                    <span
                        key={ `dot-${ i }` }
                        onClick={ () => toggleItem(i) }
                        className={`dot ${ i === activeItem ? 'active' : '' }`}
                    />
                )) }
            </div>
        </div>
    )
}