import React, { useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import s from 'styled-components'

import Input from 'UI/Input'
import Button from 'UI/Button'

import { inDesk } from 'commom/styles'

import pinterest from 'assets/images/icons/pinterest.png'
import facebook from 'assets/images/icons/facebook.png'
import google_plus from 'assets/images/icons/google_plus.png'
import twitter from 'assets/images/icons/twitter.png'

import { Creators as userActions } from 'ducks/user'

const ICONS = [ facebook, twitter, google_plus, pinterest ]

const IconsWrapper = s.div`
    margin: 40px 0px;

    ${ inDesk } {
        margin-top: 75px;
        margin-bottom: 100px;
    }
`

const Footer = ({ newsSubscrible }) => {
    const [ email, toggleEmail ] = useState("")

    return (
        <div className="row py-5">
            <div className="col-12 text-center">
                <div className="row">
                    <div id="newsletter" className="col-12 col-lg-10 mx-auto">
                        <h3 className="text-yellow mt-5 font-weight-bold f-xl"> Keep in touch with us </h3>
                        
                        <p className="mt-4 f-md text-light-gray">
                            Adipisicing incididunt irure amet minim sunt exercitation. Minim deserunt culpa quis et cupidatat ea minim nisi laboris veniam.Fugiat ullamco officia duis minim minim nisi elit exercitation ea sunt exercitation.
                        </p>

                        <div className="row justify-content-center mt-5">
                            <div className="col-12 col-lg-9">
                                <Input
                                    value={ email }
                                    onChange={ e => toggleEmail(e.target.value) }
                                    placeholder="Enter your email to update"
                                />
                            </div>

                            <div className="col-12 col-lg-2 mt-2 mt-lg-0">
                                <Button onClick={ () => newsSubscrible(email) } label="submit" />
                            </div>
                        </div>

                        <IconsWrapper className="d-flex align-items-center justify-content-center">
                            { ICONS.map((el, i) => (
                                <a href="/#books" className='mx-3' key={`icon-${ i }`}>
                                    <img src={ el } alt=""/>
                                </a>
                            ))}
                        </IconsWrapper>
                    </div>

                    <div id="address" className="col-12">
                        <div className="row justify-content-around text-white tetxe-center text-lg-left f-sm">
                            <div className="col-12 col-lg-2">
                                <p className="mb-4 text-white">
                                    Alameda Santos, 1978 <br />
                                    6th floor - Jardim Paulista <br />
                                    São Paulo - SP <br />
                                    +55 11 3090-8500
                                </p>
                            </div>

                            <div className="col-12 col-lg-2">
                                <p className="mb-4 text-white">
                                    London - UK <br />
                                    125 Kingsway <br />
                                    London WC2B 6NH
                                </p>
                            </div>

                            <div className="col-12 col-lg-2">
                                <p className="mb-4 text-white">
                                    Lisbon - Portugal <br />
                                    Rua Rodrigues Faria, 103 <br />
                                    4tg floor <br />
                                    Lisbon - Portugal
                                </p>
                            </div>

                            <div className="col-12 col-lg-2">
                                <p className="mb-4 text-white">
                                    Curitiba - PR <br />
                                    R. Francisco Rocha, 198 <br />
                                    Batel - Curitiba - PR
                                </p>
                            </div>

                            <div className="col-12 col-lg-2">
                                <p className="mb-4 text-white">
                                    Buenos Aires - Argentina <br />
                                    Esmeralda 950 <br />
                                    Buenos Aires B C1007
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapDispatch = dispatch => bindActionCreators({
    ...userActions
}, dispatch)

export default connect(null, mapDispatch)(Footer)