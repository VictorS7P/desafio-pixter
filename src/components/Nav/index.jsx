import "./Nav.scss"
import React, { useState } from 'react'
import { NavHashLink as NavLink } from 'react-router-hash-link'

import logo from 'assets/images/logo.png'
import menuIcon from 'assets/images/icons/menu.png'

const LINKS = [
    {
        label: 'Books',
        link: '/#books',
    }, {
        label: 'Newsletter',
        link: '/#newsletter',
    }, {
        label: 'Address',
        link: '/#address',
    }, 
]

export default () => {
    const [ showMenu, toggleMenu ] = useState(0)

    return (
        <nav className="row navbar-expand-lg navbar-light py-4">
            <div className="col-12 col-lg-7">
                <div className="d-flex align-items-center">
                    <NavLink className="navbar-brand" to="/">
                        <img src={ logo } alt="Pixter" />
                    </NavLink>

                    <div className="ml-auto">
                        <div className="d-flex align-items-center d-lg-none" onClick={ () => toggleMenu(!showMenu) }>
                            <div className="mr-3">
                                <img src={ menuIcon } alt=""/>
                            </div>

                            <span className="f-lg"> Menu </span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="col-12 col-lg-5">
                <div className={`navbar-collapse collapse ${ showMenu ? 'show' : '' }`}>
                    <div className="navbar-nav d-lg-flex w-100 justify-content-between">
                        { LINKS.map((el, i) => (
                            <NavLink smooth to={ el.link } exact key={`link-${i}`} className="nav-link f-md font-weight-bold text-black">
                                { el.label }
                            </NavLink>
                        ))}
                    </div>
                </div>
            </div>
        </nav>
    )
}