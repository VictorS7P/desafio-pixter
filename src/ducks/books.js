import { createActions, createReducer } from 'reduxsauce'
import { success, loading } from 'commom/functions'

export const { Types, Creators } = createActions({
    listRequest: ["search"],
    listRequestSuccess:["list"],
    listRequestFailed: ["errors"],
})

const INITIAL_STATE = {
    list: [],
    listLoading: false,
    errors: []
}

export default createReducer(INITIAL_STATE, {
    [Types.LIST_REQUEST]: state => loading(state, 'listLoading'),
    [Types.LIST_REQUEST_SUCCESS]: (state, values) => success(state, values, 'listLoading'),
    [Types.LIST_REQUEST_FAILED]: (state, values) => success(state, values, 'listLoading'),
})