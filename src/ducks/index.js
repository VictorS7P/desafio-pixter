import { combineReducers } from 'redux'

import books from 'ducks/books'
import user from 'ducks/user'

export default combineReducers({
    books,
    user
})