import { createActions, createReducer } from 'reduxsauce'
import { failed, success, loading } from 'commom/functions'

export const { Types, Creators } = createActions({
    newsSubscrible: ["email"],
    newsSubscribleSuccess: null,
    newsSubscribleFailed: ["errors"],
})

const INITIAL_STATE = {
    loading: false
}

export default createReducer(INITIAL_STATE, {
    [Types.NEWS_SUBSCRIBLE]: loading,
    [Types.NEWS_SUBSCRIBLE_SUCCESS]: success,
    [Types.NEWS_SUBSCRIBLE_FAILED]: failed,
})