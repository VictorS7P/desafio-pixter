/*
 * TO USE IN ACTIONS
 */
export const success = (state, values, loadingKey = 'loading') => ({
    ...state,
    ...values,
    [loadingKey]: false,
    errors: []
})

export const failed = (state, { errors }, loadingKey = 'loading') =>({
    ...state,
    [loadingKey]: false,
    errors: errors || []
})

export const loading = (state, loadingKey = 'loading') => ({
    ...state,
    [loadingKey]: true,
    errors: []
})

/*
 * DOM HELPERS
 */
export const setScroll = status => {
    document.body.style.overflow = status ? 'inherit' : 'hidden';
}