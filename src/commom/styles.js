/*
 * APP COLORS
 */
export const yellow = '#FCDB00';
export const black = '#010101';
export const white = '#FEFEFE';
export const darkGray = '#232527';
export const lightGray = '#898989';

export const COLORS = {
    
}

/*
 * RESPONSIVE COMMON's
 */
export const inMob = "@media screen and (max-width: 992px)";
export const inDesk = "@media screen and (min-width: 992px)";