import { all, fork } from 'redux-saga/effects'

import books from './books'
import user from './user'

export default function* root() {
    yield all([
        fork(books),
        fork(user),
    ])
}