import { all, takeLatest, put } from 'redux-saga/effects'
import { handleError } from './commom'

function* newsSubscrible({ email }) {
    try {
        // const req = yield axios.get(`${ API_URL }${ search }`)

        window.alert(`Newsletter para o email ${ email } assinada com sucesso!`)

        yield put({
            type: 'NEWS_SUBSCRIBLE_SUCCESS',
        })
    } catch(error) {
        handleError(error)

        yield put({
            type: 'NEWS_SUBSCRIBLE_FAILED',
            errors: []
        })
    }
}

export default function* () {
    yield all([
        takeLatest('NEWS_SUBSCRIBLE', newsSubscrible),
    ])
}