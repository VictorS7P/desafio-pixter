import { all, takeLatest, put } from 'redux-saga/effects'
import axios from 'axios'
import { API_URL } from 'commom/consts'
import { handleError } from './commom'

function* listRequest({ search }) {
    try {
        const req = yield axios.get(`${ API_URL }${ search }`)

        yield put({
            type: 'LIST_REQUEST_SUCCESS',
            list: req.data.items
        })
    } catch(error) {
        handleError(error)

        yield put({
            type: 'LIST_REQUEST_FAILED',
            errors: ['Falha ao buscar lista']
        })
    }
}

export default function* () {
    yield all([
        takeLatest('LIST_REQUEST', listRequest),
    ])
}