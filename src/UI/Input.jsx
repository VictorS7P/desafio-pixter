import React from 'react'
import s from 'styled-components'

const Input = s.input`
    height: 50px;
    width: 100%;
    border-radius: 3px;
    outline: none;
    border: none;
    padding: 5px 20px;
`

export default props => (
    <Input { ...props } />
)