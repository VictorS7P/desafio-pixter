import React, { useRef, useEffect } from 'react'
import s from 'styled-components'

export const Wrapper = s.div`
    width: 100vw;
    height: 100vh;
    transition: all .3s ease;

    position: fixed;
    top: 0px; left: 0px;
    overflow: hidden;

    background: #00000099;
    display: flex;
    align-items: center;
    justify-content: center;

    z-index: 10;
`

export const Modal = s.div`
    width: 500px;
    max-width: 90vw;
    height: 400px;
    max-height: 90vh;
    position: relative;
    overflow: hidden;
    transition: all .3s ease;

    background: white;
    border-radius: 3px;
    box-shadow: 2px 2px 4px #00000055;
`

export const Close = s.span`
    position: absolute;
    top: 13px;
    right: 18px;
`
const wrapperStyle = {
    entered:  { display: 'flex', opacity: '1' },
    entering: { display: 'flex', opacity: '0' },
    exiting:  { display: 'flex', opacity: '0' },
    exited:   { display: 'none', opacity: '0' }
}

const modalStyle = {
    entered:  { display: 'block', transform: 'scale(1)' },
    entering: { display: 'block', transform: 'scale(0)' },
    exiting:  { display: 'block', transform: 'scale(0)' },
    exited:   { display: 'none',  transform: 'scale(0)' }
}

export default ({ children, close, state = 'entered' }) => {
    const wrapper = useRef()
    const modal   = useRef() 

    useEffect(() => {
        const listener = wrapper.current.addEventListener("click", (event) => {
            if(!event.path.includes(modal.current)) {
                close()
            }
        })

        return () => {
            document.removeEventListener(listener)
        }
    }, [])

    return (
        <Wrapper ref={ wrapper } style={ wrapperStyle[state] }>
            <Modal ref={ modal } style={ modalStyle[state] }>
                { children }
            </Modal>
        </Wrapper>
    )
}