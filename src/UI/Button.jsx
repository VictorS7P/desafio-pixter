import React from 'react'
import s from 'styled-components'

import { yellow, black } from 'commom/styles'

const Button = s.button`
    height: 50px;
    width: 100%;
    border-radius: 3px;
    outline: none;
    border: none;
    padding: 5px 20px;
    background: ${ yellow };
    color: ${ black };
`

export default props => (
    <Button { ...props }>
        <span className="text-uppercase f-sm font-weight-bold">
            { props.label }
        </span>
    </Button>
)